jest.mock('express');
let express = require('express');
const get = jest.fn();
const listen = jest.fn();

express.mockReturnValue({
  get,
  listen,
});

it('should declare and endpoint and start a server', () => {
  require('../index');

  expect(express).toHaveBeenCalledTimes(1);
  expect(get).toHaveBeenCalledTimes(1);
  expect(listen).toHaveBeenCalledTimes(1);
});