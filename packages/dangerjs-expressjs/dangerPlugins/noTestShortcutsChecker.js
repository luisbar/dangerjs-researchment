const noTestShortcuts = require('danger-plugin-no-test-shortcuts');
const path = require('path');
const { isCI } = require('ci-info');

module.exports = () => {
  const modifiedFiles = [...danger.git.modified_files];

  if (!isCI)
    danger.git.modified_files = danger.git.modified_files.map(fileRelativePath => path.resolve(process.cwd(), `../../${fileRelativePath}`));

  noTestShortcuts.default({
    testFilePredicate: (filePath) => {
      return filePath.endsWith('.test.js');
    }
  });

  if (!isCI)
    danger.git.modified_files = modifiedFiles;
}