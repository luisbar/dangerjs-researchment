const madge = require('madge');
const ciInfo = require('ci-info');
const fs = require('fs');
const FormData = require('form-data');
const axios = require('axios');

module.exports = async () => {
  let madgePath = `${process.cwd()}/src/index.js`;

  if (ciInfo.isCI)
    madgePath = `${process.cwd()}/packages/dangerjs-expressjs/src/index.js`;

  const result = await madge(madgePath);

  if (!result.circular().length) return;

  const imagePath = await result.image('./circular-dependencies.png');
  const data = new FormData();
  data.append('file', fs.createReadStream(imagePath));

  let config = {
    url: `https://gitlab.com/api/v4/projects/${process.env.PROJECT_ID}/uploads`,
    method: 'POST',
    headers: { 
      'Content-Type': 'multipart/form-data', 
      'PRIVATE-TOKEN': process.env.DANGER_GITLAB_API_TOKEN, 
    },
    data: data,
  };

  const { data: { markdown: image } } = await axios.request(config)
  fs.rmSync(imagePath);

  fail(`Circular dependencies detected:\n\n ${image}`);
}; 