const conventionalCommitChecker = require('danger-plugin-conventional-commitlint').default;
const configConventional = require('@commitlint/config-conventional');

module.exports = async () => {
  await conventionalCommitChecker(configConventional.rules, {
    severity: 'fail',
  });
};