module.exports = () => {
  const packageChanged = danger.git.modified_files.map(filePath => filePath.split('/').pop()).includes('package.json');
  const lockfileChanged = danger.git.modified_files.map(filePath => filePath.split('/').pop()).includes('yarn.lock');
  
  if ((packageChanged && lockfileChanged) || !packageChanged) return;

  let failureMessage = 'Changes were made to package.json, but not to yarn.lock';
  const reasons = ['Perhaps you need to run `yarn install`?', 'Perhaps you forgot to add the `yarn.lock` file?'];

  reasons.forEach(reason => {
    failureMessage = `${failureMessage}\n- ${reason}`;
  });

  fail(failureMessage);
};