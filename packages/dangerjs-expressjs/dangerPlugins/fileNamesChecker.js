const Case = require('case');

module.exports = () => {
  const createdFiles = danger.git.created_files
  .filter(filePath => filePath.endsWith('.js'))
  .reduce((accumulator, filePath) => {
    accumulator[filePath.split('/')
    .pop()
    .replace('.js', '')] = filePath; return accumulator;
  }, {});

  const filesWithWrongNames = Object
  .entries(createdFiles)
  .map(([fileName, filePath]) => {
    if (Case.of(fileName) !== 'camel' && Case.of(fileName) !== 'lower') return filePath;

    return null;
  })
  .filter(Boolean);

  if (!filesWithWrongNames.length) return;
  
  let failureMessage = 'Following files have wrong names:\n ';
  filesWithWrongNames
  .forEach(filePath => { failureMessage = `${failureMessage}\n- ${filePath}`; });
  fail(failureMessage);
};