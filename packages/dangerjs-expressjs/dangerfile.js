const {
  noTestShortcutsChecker,
  conventionalCommitChecker,
  updatedLockfileChecker,
  fileNamesChecker,
  circularDependenciesChecker,
} = require('./dangerPlugins');

(async () => {
  noTestShortcutsChecker();
  await conventionalCommitChecker();
  updatedLockfileChecker();
  fileNamesChecker();
  await circularDependenciesChecker();
})();