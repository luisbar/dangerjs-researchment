This is a monorepo which has examples about Danger

## How to run it?
- Create a new branch and break the code in order to see a message in the PR, right now we have the following scripts:
  ```bash
    ├── dangerPlugins
    │   ├── circularDependenciesChecker.js
    │   ├── conventionalCommitChecker.js
    │   ├── fileNamesChecker.js
    │   ├── index.js
    │   ├── noTestShortcutsChecker.js
    │   └── updatedLockfileChecker.js
  ```

## How to add a new one?
- Create a script into `dangerPlugins` folder and import it in `danjerfile.js`
## Hints
- If you want to run Danger locally, you can do it by using two commands
  - yarn danger pr
    ```bash
    DANGER_GITLAB_API_TOKEN=YOUR_GITLAB_TOKEN DANGER_GITLAB_HOST=gitlab.com DANGER_GITLAB_API_BASE_URL=https://gitlab.com/api/v4 PROJECT_ID=YOUR_PROJECT_ID npx lerna exec yarn danger pr https://gitlab.com/GITLAB_USERNAME/REPO_NAME/-/merge_requests/PULL_REQUEST_ID
    ```
  - yarn danger ci
    ```bash
    DANGER_GITLAB_API_TOKEN=YOUR_GITLAB_TOKEN DANGER_GITLAB_HOST=gitlab.com DANGER_GITLAB_API_BASE_URL=https://gitlab.com/api/v4 DANGER_FAKE_CI=YES DANGER_TEST_REPO=GITLAB_USERNAME/REPO_NAME DANGER_TEST_PR=PULL_REQUEST_ID PROJECT_ID=YOUR_PROJECT_ID npx lerna exec yarn danger ci
    ```
    - If you receive not found is because the identifier of your PR is incorrect
- You have access to Github and Gitlab API through `danger` object
- Do not forget to install [Graphviz](https://graphviz.org) if you want to use the `svg` method of [Madge](https://www.npmjs.com/package/madge)